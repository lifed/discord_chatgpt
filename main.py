import nest_asyncio
import discord
import openai
import os
from dotenv import load_dotenv

nest_asyncio.apply()
load_dotenv()

def response(prompt):
    return openai.Completion.create(
        engine=model_engine
        , prompt=prompt
        , top_p=top_p
        , max_tokens=max_tokens
        , frequency_penalty=frequency_penalty
        , presence_penalty=presence_penalty
        )["choices"][0]["text"].strip()
  
# Discord key and channel ID
DiscordBotToken = os.getenv('DiscordBotToken')
DiscordServerChannel_ID = int(os.getenv('DiscordServerChannel_ID'))
# OpenAI Token
openai.api_key = os.getenv('OpenAIToken')
# OpenAI parameter
max_tokens=int(os.getenv('max_tokens'))
top_p=float(os.getenv('top_p'))
frequency_penalty=float(os.getenv('frequency_penalty'))
presence_penalty=float(os.getenv('presence_penalty'))
# TODO: 可透過指令選擇適當的模組
model_engine = 'text-davinci-003'

intents = discord.Intents.default()
intents.message_content = True
client = discord.Client(intents=intents)

@client.event
async def on_message(message):
    if message.author == client.user:
        return
    if message.channel.id == DiscordServerChannel_ID:
        replymsg = '無回應'
        async with message.channel.typing():
            print(f'等待回應內容：{message.content}')
            try:
                replymsg = response(message.content)
            except Exception as e:
                replymsg = e
                print(f'錯誤內容：{e}')
        print(f'完成回應內容：{replymsg}')
        await message.reply(replymsg)

@client.event
async def on_ready():
    print(f'目前登入身份：{client.user}, id：{client.user.id}')

client.run(DiscordBotToken)

