## .env
請先申請Tokens，放入對應的變數中。  
1. [OpenAI Token申請網址](https://platform.openai.com/account/api-keys)  
2. [Discord LogoDeveloper Portal](https://discord.com/developers/applications)
3. 需要透過Discord程式開啟開發者模式，對頻道右鍵即可選擇複製頻道ID

結果如下(僅供參考)
```bash
OpenAIToken=sk-kRoB0I699999999999999999999999999999999Kcn1BKz
DiscordBotToken=MTA3MTA5ODgzM9999999999999999999999999910LKrkDBIpFdHs
DiscordServerChannel_ID=10711099999999999613
```

## 安裝
如果上面的變數都湊不齊，請不用繼續往下看。
### Python 3
請先至官方網站抓python3

#### 安裝requirements
請先自行查看一下requirements.txt，安裝的套件是不是有問題
```bash
pip install -r requirements.txt
```

## 啟動
```bash
python main.py
```